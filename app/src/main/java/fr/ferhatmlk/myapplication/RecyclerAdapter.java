package fr.ferhatmlk.myapplication;



import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import fr.ferhatmlk.myapplication.data.Country;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {



    String uri = "@drawable/name_file";
    Context c = view.getContext();
    view.setImageDrawable(c.getResources().getDrawable(
                                                       c.getResources(). getIdentifier (uri, null , c.getPackageName())));

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.card_layout, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.itemPays.setText(Country.countries[i].getName());
        viewHolder.itemCapitale.setText(Country.countries[i].getCapital());
        viewHolder.itemImage.setImageResource(images[i]);
    }

    @Override
    public int getItemCount() {
        return pays.length;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView itemImage;
        TextView itemPays;
        TextView itemCapitale;

        ViewHolder(View itemView) {
            super(itemView);
            itemImage = itemView.findViewById(R.id.item_image);
            itemPays = itemView.findViewById(R.id.item_pays);
            itemCapitale = itemView.findViewById(R.id.item_capitale);

            int position = getAdapterPosition();

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {

                    int position = getAdapterPosition();


                    Navigation.findNavController(v).navigate(androidx.navigation.safeargson);
                }
            });

        }
    }

}